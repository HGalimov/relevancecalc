import java.io.*;
import java.util.*;

public class RelevanceCalc {
    public static void main (String args[]) {
        String q = ""; //запрос
        double tf_t_d; //частота термина в документе
        double d_length; //длина документа - количество слов в документе
        double avdl; //средняя длина документа
        double tf_t_q; //частота термина в запросе
        int n = 5; //количество документов
        double df_t; //документная частота термина
        double s = 0.5D;
        double score = 0; //очки ранжирования, релевантность
        try {
            File f[] = new File[n];
            for (int i = 0; i < f.length; i++) {
                int nm = i + 1;
                f[i] = new File(System.getProperty("user.dir") + "\\src\\" + nm + ".txt");
            }
            String st = "";
            Map<String, ArrayList<Integer>> words = new TreeMap<>();
            for (int i = 0; i < f.length; i++) {
                BufferedReader br = new BufferedReader(new FileReader(f[i]));
                while ((st = br.readLine()) != null) {
                    if (st.length() == 0) continue;
                    st = st.toLowerCase();
                    st = st.replace(",", "");
                    st = st.replace(".", "");
                    st = st.replace(";", "");
                    st = st.replace(":", "");
                    st = st.replace("(", "");
                    st = st.replace(")", "");
                    st = st.replace("—", "");
                    st = st.replace("- ", "");
                    st = st.replace(" - ", "");
                    st = st.replace(" —", "");
                    st = st.replace(" — ", "");
                    st = st.replace("\"", "");
                    String str[] = st.split(" ");

                    for (int k = 0; k < str.length; k++) {
                        if (!words.containsKey(str[k])) {
                            ArrayList<Integer> docId = new ArrayList<>();
                            docId.add(i + 1);
                            words.put(str[k], docId);
                        } else {
                            ArrayList<Integer> docId = words.get(str[k]);
                            docId.add(i+1);
                            words.put(str[k], docId);
                        }
                    }
                }
            }
            Iterator iterator1 = words.keySet().iterator();
            while (iterator1.hasNext()) {
                String key = iterator1.next().toString();
                String value = "";
                for (int i = 0; i < words.get(key).size(); i++)
                    value += " " + words.get(key).get(i);
                System.out.println(key + " " + value);
            }
            BufferedReader br_q = new BufferedReader(new InputStreamReader(System.in));
            q = br_q.readLine();
            q = q.toLowerCase();
            q = q.replace(",", "");
            q = q.replace(".", "");
            q = q.replace(";", "");
            q = q.replace(":", "");
            q = q.replace("(", "");
            q = q.replace(")", "");
            q = q.replace("—", "");
            q = q.replace("- ", "");
            q = q.replace(" - ", "");
            q = q.replace(" —", "");
            q = q.replace(" — ", "");
            q = q.replace("\"", "");
            String t_s[] = q.split(" ");

            List<String> uniq_t_s = new LinkedList<>();
            for (int i = 0; i < t_s.length; i++)
                if(!uniq_t_s.contains(t_s[i]))
                    uniq_t_s.add(t_s[i]);
            System.out.println("term" + " docId" + " Score");

            Map <Integer,Double> total = new TreeMap<>();

            for (int j = 0; j < uniq_t_s.size(); j++) {
                avdl = AvDocLength(words, n);
                tf_t_q = QFreq(t_s, t_s[j]);
                df_t = CountDoc(words,uniq_t_s.get(j));
                for (int i = 1; i < n+1; i++) {
                    tf_t_d = DocFreq(words, uniq_t_s.get(j), i);
                    d_length = DocLength(words, i);
                    if (tf_t_d != 0)
                        score = (1 + Math.log(tf_t_d)/((1 - s) + s * (d_length/avdl)) * tf_t_q *
                                Math.log((n+1)/df_t + 0.5));
                    else score = 0;
                    if (!total.containsKey(i))
                        total.put(i, score);
                    else total.put(i, total.get(i) + score);

                    System.out.println(uniq_t_s.get(j) + " " + i + " " + score);
                }

            }
            System.out.println("\n" + "docId " + "Score");

            for(Map.Entry<Integer, Double> pair : total.entrySet())
            {
                System.out.println(pair.getKey() + " " + pair.getValue());
            }
            

        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }

    }

    /*Вычисление длины одного документа*/
    public static double DocLength(Map<String, ArrayList<Integer>> words, int docId) {
        int length = 0;
        Iterator iterator1 = words.keySet().iterator();
        while (iterator1.hasNext()) {
            String key = iterator1.next().toString();
            for (int i = 0; i < words.get(key).size(); i++)
                if (words.get(key).get(i) == docId)
                    length++;
        }
        return length;
    }

    /*Вычисление средней длины документов*/
    public static double AvDocLength(Map<String, ArrayList<Integer>> words, int n) {
        int length = 0;
        Iterator iterator1 = words.keySet().iterator();
        while (iterator1.hasNext()) {
            String key = iterator1.next().toString();
            for (int i = 0; i < words.get(key).size(); i++)
                length++;
        }
        return  length/n;
    }

    /*Вычисление частоты термина в документе*/
    public static double DocFreq (Map<String, ArrayList<Integer>> words, String t, int docId) {
        int docfreq = 0;
        Iterator iterator1 = words.keySet().iterator();
        while (iterator1.hasNext()) {
            String key = iterator1.next().toString();
            if (key.equals(t)) {
                for (int i = 0; i < words.get(key).size(); i++)
                    if (words.get(key).get(i) == docId)
                        docfreq++;
            }
        }
        return docfreq;
    }

    /*Вычисление документной частоты*/
    public static double CountDoc (Map<String, ArrayList<Integer>> words, String t) {
        int count = 0;
        Iterator iterator1 = words.keySet().iterator();
        while (iterator1.hasNext()) {
            String key = iterator1.next().toString();
            if (key.equals(t)) {
                int a = 0;
                for (int i = 0; i < words.get(key).size(); i++) {
                    if (words.get(key).get(i) != a) {
                        count++;
                        a = words.get(key).get(i);
                    }
                }
            }
        }
        return count;
    }

    /*Вычисление частоты термина в запросе*/
    public static double QFreq (String[] t_s, String t) {
        int qfreq = 0;
        for (int i = 0; i < t_s.length; i++) {
            if (t_s[i].equals(t))
                qfreq++;
        }
        return qfreq;
    }
}